package com.cata.sg.catasg.service;

import com.cata.sg.catasg.common.exception.AccountNotFoundException;
import com.cata.sg.catasg.common.exception.BadRequestException;
import com.cata.sg.catasg.common.exception.UnauthorizedAmountException;
import com.cata.sg.catasg.data.AccountRepository;
import com.cata.sg.catasg.data.OperationRepository;
import com.cata.sg.catasg.domain.Account;
import com.cata.sg.catasg.domain.DateRange;
import com.cata.sg.catasg.domain.Operation;
import com.cata.sg.catasg.domain.OperationType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OperationServiceTest {

    @InjectMocks
    OperationService operationService;

    @Mock
    AccountRepository accountRepository;

    @Mock
    OperationRepository operationRepository;

    @Test(expected = AccountNotFoundException.class)
    public void when_account_not_found_expect_exception() throws AccountNotFoundException, UnauthorizedAmountException {

        //given
        String accountId = "accountId";
        Operation operation = Operation.builder().build();

        when(accountRepository.findAccountById(accountId)).thenReturn(null);

        //when
        operationService.executeOperation(accountId, operation);

        //then
        Assert.fail("An AccountNotFoundException should have been thrown !");
    }

    @Test(expected = UnauthorizedAmountException.class)
    public void when_try_to_depot_unauthorized_amount_expect_exception() throws AccountNotFoundException, UnauthorizedAmountException {

        //given
        String accountId = "accountId";
        int depotAmount = 5500;
        Operation operation = Operation.builder().amount(depotAmount).type(OperationType.DEPOSITION).build();

        when(accountRepository.findAccountById(accountId)).thenReturn(Account.builder().id(accountId).build());

        //when
        operationService.executeOperation(accountId, operation);

        //then
        Assert.fail("An UnauthorizedAmountException should have been thrown !");
    }

    @Test
    public void when_try_to_depot_valid_amount_expect_valid_operation() throws AccountNotFoundException, UnauthorizedAmountException {

        //given
        String accountId = "accountId";
        int depotAmount = 3500;
        Operation operation = Operation.builder().amount(depotAmount).type(OperationType.DEPOSITION).build();

        when(accountRepository.findAccountById(accountId)).thenReturn(Account.builder().id(accountId).build());
        Operation _operation = operation;
        _operation.setAccountId(accountId);
        when(operationRepository.saveOperation(_operation)).thenReturn(_operation);
        when(accountRepository.addDeposition(accountId, depotAmount)).thenReturn(depotAmount);

        //when
        Operation result = operationService.executeOperation(accountId, operation);

        //then
        assertThat(result).isNotNull();
        verify(operationRepository, times(1)).saveOperation(_operation);
        verify(accountRepository, times(1)).addDeposition(accountId, depotAmount);

    }

    @Test(expected = UnauthorizedAmountException.class)
    public void when_try_to_withdraw_unauthorized_amount_expect_exception() throws AccountNotFoundException, UnauthorizedAmountException {

        //given
        String accountId = "accountId";
        int withdrawAmount = 1000;
        int accountCreditAmount = 300;
        Operation operation = Operation.builder().amount(withdrawAmount).type(OperationType.WITHDRAWAL).build();

        when(accountRepository.findAccountById(accountId)).thenReturn(Account.builder().id(accountId).build());
        when(accountRepository.getAccountCredit(accountId)).thenReturn(accountCreditAmount);

        //when
        operationService.executeOperation(accountId, operation);

        //then
        Assert.fail("An UnauthorizedAmountException should have been thrown !");
    }

    @Test
    public void when_try_to_withdraw_valid_amount_expect_valid_operation() throws AccountNotFoundException, UnauthorizedAmountException {

        //given
        String accountId = "accountId";
        int withdrawAmount = 500;
        int accountCreditAmount = 300;
        Operation operation = Operation.builder().amount(withdrawAmount).type(OperationType.WITHDRAWAL).build();

        when(accountRepository.findAccountById(accountId)).thenReturn(Account.builder().id(accountId).build());
        when(accountRepository.getAccountCredit(accountId)).thenReturn(accountCreditAmount);
        Operation _operation = operation;
        _operation.setAccountId(accountId);
        when(operationRepository.saveOperation(_operation)).thenReturn(_operation);
        when(accountRepository.subtructWithdrawal(accountId, withdrawAmount)).thenReturn(withdrawAmount);

        //when
        Operation result = operationService.executeOperation(accountId, operation);

        //then
        assertThat(result).isNotNull();
        verify(operationRepository, times(1)).saveOperation(_operation);
        verify(accountRepository, times(1)).subtructWithdrawal(accountId, withdrawAmount);
    }

    @Test(expected = AccountNotFoundException.class)
    public void when_account_not_found_for_get_operations_expect_exception() throws AccountNotFoundException, BadRequestException {

        //given
        String accountId = "accountId";
        long startDate = 1518651034;
        long endDate = 1518656400;
        DateRange dateRange = DateRange.builder()
                                        .startDate(startDate)
                                        .endDate(endDate)
                                        .build();

        when(accountRepository.findAccountById(accountId)).thenReturn(null);

        //when
        operationService.getOperations(accountId,dateRange);

        //then
        Assert.fail("An AccountNotFoundException should have been thrown !");

    }

    @Test(expected = BadRequestException.class)
    public void when_wrong_params_expect_exception() throws AccountNotFoundException, BadRequestException {

        //given
        String accountId = "accountId";
        long startDate = 1518656400;
        long endDate = 1518651034;
        DateRange dateRange = DateRange.builder()
                .startDate(startDate)
                .endDate(endDate)
                .build();

        when(accountRepository.findAccountById(accountId)).thenReturn(Account.builder().id(accountId).build());

        //when
        operationService.getOperations(accountId, dateRange);

        //then
        Assert.fail("An BadRequestException should have been thrown !");

    }

    @Test
    public void when_valid_params_return_operation_list() throws AccountNotFoundException, BadRequestException {

        //given
        String accountId = "accountId";
        long startDate = 1518651034;
        long endDate = 1518656400;
        DateRange dateRange = DateRange.builder()
                .startDate(startDate)
                .endDate(endDate)
                .build();

        List<Operation> operations = new ArrayList<>();
        operations.add(Operation.builder().id("Op-1").build());
        operations.add(Operation.builder().id("Op-2").build());
        when(operationRepository.getOperations(accountId, dateRange)).thenReturn(operations);

        when(accountRepository.findAccountById(accountId)).thenReturn(Account.builder().id(accountId).build());

        //when
        operationService.getOperations(accountId, dateRange);

        //then
        verify(accountRepository, times(1)).findAccountById(accountId);
        verify(operationRepository, times(1)).getOperations(accountId, dateRange);

    }

}