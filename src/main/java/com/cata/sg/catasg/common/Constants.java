package com.cata.sg.catasg.common;

public class Constants {

    public final static int MAXIMUM_DEPOT_AMOUNT = 5000;
    public final static int MAXIMUM_EXPOSURE_AMOUNT = -500;
}
