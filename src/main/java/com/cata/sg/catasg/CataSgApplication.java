package com.cata.sg.catasg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CataSgApplication {

	public static void main(String[] args) {
		SpringApplication.run(CataSgApplication.class, args);
	}
}
