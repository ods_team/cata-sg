package com.cata.sg.catasg.service;

import com.cata.sg.catasg.common.exception.AccountNotFoundException;
import com.cata.sg.catasg.common.exception.BadRequestException;
import com.cata.sg.catasg.common.exception.UnauthorizedAmountException;
import com.cata.sg.catasg.data.AccountRepository;
import com.cata.sg.catasg.data.OperationRepository;
import com.cata.sg.catasg.domain.DateRange;
import com.cata.sg.catasg.domain.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.cata.sg.catasg.common.Constants.MAXIMUM_DEPOT_AMOUNT;
import static com.cata.sg.catasg.common.Constants.MAXIMUM_EXPOSURE_AMOUNT;

@Service
public class OperationService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    OperationRepository operationRepository;

    public Operation executeOperation(String accountId, Operation operation) throws AccountNotFoundException, UnauthorizedAmountException {

        if(accountRepository.findAccountById(accountId) == null)
            throw new AccountNotFoundException();

        switch (operation.getType()) {
            case DEPOSITION:
                return executeDepositionOperation(accountId, operation);
            case WITHDRAWAL:
                return executeWithdrawalOperation(accountId, operation);
            default:
                return null;
        }

    }

    private Operation executeDepositionOperation(String accountId, Operation operation) throws UnauthorizedAmountException {

        if(operation.getAmount() > MAXIMUM_DEPOT_AMOUNT)
            throw new UnauthorizedAmountException();

        operation.setAccountId(accountId);
        Operation result = operationRepository.saveOperation(operation);
        accountRepository.addDeposition(accountId, operation.getAmount());

        return result;

    }

    private Operation executeWithdrawalOperation(String accountId, Operation operation) throws UnauthorizedAmountException {

        int creditAmount = accountRepository.getAccountCredit(accountId);

        if((creditAmount - operation.getAmount()) < MAXIMUM_EXPOSURE_AMOUNT)
            throw new UnauthorizedAmountException();

        operation.setAccountId(accountId);
        Operation result = operationRepository.saveOperation(operation);
        accountRepository.subtructWithdrawal(accountId, operation.getAmount());

        return result;
    }

    public List<Operation> getOperations(String accountId, DateRange dateRange) throws AccountNotFoundException, BadRequestException {

        if(accountRepository.findAccountById(accountId) == null)
            throw new AccountNotFoundException();

        if(!dateRange.isValid())
            throw new BadRequestException();

        return operationRepository.getOperations(accountId, dateRange);

    }

}