package com.cata.sg.catasg.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DateRange {
    private long startDate;
    private long endDate;

    public boolean isValid() {
        return startDate < endDate;
    }
}
