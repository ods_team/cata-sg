package com.cata.sg.catasg.domain;

public enum OperationType {
    WITHDRAWAL,
    DEPOSITION
}
