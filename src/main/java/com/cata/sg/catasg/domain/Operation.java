package com.cata.sg.catasg.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Operation {

    private String id;
    private String accountId;
    private OperationType type;
    private int amount;
    private long date;

}
