package com.cata.sg.catasg.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Account {

    private String id;

}
