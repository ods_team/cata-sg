package com.cata.sg.catasg.data;

import com.cata.sg.catasg.domain.Account;
import org.springframework.stereotype.Repository;

import static com.cata.sg.catasg.common.Constants.MAXIMUM_DEPOT_AMOUNT;

@Repository
public class AccountRepository {

    public Account findAccountById(String id) {
        return Account.builder().id(id).build();
    }

    public Integer getAccountCredit(String id) {
        return MAXIMUM_DEPOT_AMOUNT;
    }

    public Integer addDeposition(String id, int amount) {
        return amount;
    }

    public Integer subtructWithdrawal(String id, int amount) {
        return amount;
    }
}
