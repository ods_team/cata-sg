package com.cata.sg.catasg.data;

import com.cata.sg.catasg.domain.DateRange;
import com.cata.sg.catasg.domain.Operation;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OperationRepository {

    public Operation saveOperation(Operation operation) {
        return operation;
    }

    public List<Operation> getOperations(String accountId, DateRange dateRange) {
        return new ArrayList<>();
    }
}
